import React from "react";

import { Link, useNavigate } from "react-router-dom";
import { Avatar, Badge, Button, Drawer, Image, Modal } from "antd";
import {
  DeleteOutlined,
  MinusCircleFilled,
  PlusCircleFilled,
  ShoppingCartOutlined,
} from "@ant-design/icons";
import { Auth, HeaderItem } from "../../utlis/items";
import image1 from "../../image/Logo.png";
import UserHeader from "./UserHeader";

import { useAppContext } from "../../ContextApi";

const Index = () => {
  const { appState, updateState } = useAppContext();
  console.log("appState", appState);
  const [myOrder, setMyOrder] = React.useState([]);

  const navigate = useNavigate();
  const [open, setOpen] = React.useState(false);

  const handleClick = (e) => {
    console.log("data", e);
    navigate(e);
  };

  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  const handleAdd = (id) => {
    console.log("myOrder", myOrder);
    const updateAdddata = myOrder?.map((item) => {
      if (id === item.id) {
        return {
          ...item,
          qty: item.qty + 1,
        };
      } else {
        return {
          ...item,
        };
      }
    });
    setMyOrder(updateAdddata);
    console.log("add", updateAdddata);
  };
  const handleMinus = (id) => {

console.log("minus", id);
  };
  const handleDelete = (id) => {
    const updatedDeletedata = myOrder.filter((item) => item.id !== id);
    setMyOrder(updatedDeletedata);
    updateState({ addtocard: updatedDeletedata });
  };
  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  React.useEffect(() => {
    setMyOrder([...new Set(appState.addtocard)]);
  }, [appState.addtocard]);
  return (
    <div className="flex justify-between">
      <div>
        <Link to="/">
          <img
            style={{
              width: "50px",
              height: "60px",
              paddingTop: "15px",
            }}
            src={image1}
            alt=""
          />
        </Link>
      </div>

      <div className="flex gap-10">
        {HeaderItem?.map((item) => (
          <div
            key={item.link}
            className="text-white font-bold"
            onClick={() => handleClick(item.link)}
          >
            {item.name}
          </div>
        ))}
      </div>
      <div className="flex gap-5 items-center">
        <div className="mt-[5px]">
          <Badge count={myOrder.length}>
            <ShoppingCartOutlined
              className=" text-white text-2xl  "
              onClick={showDrawer}
            />
          </Badge>
        </div>
        {open && (
          <div>
            <Drawer title="Your Cart items" onClose={onClose} open={open}>
              <div className="gap-4 grid">
                {myOrder?.map((item) => (
                  <div key={item.id} className="flex items-center gap-3">
                    <div className="w-20 h-20">
                      <Image src={item.image} alt="noimage" />
                    </div>
                    <div>
                      {" "}
                      <div>Name:{item.name}</div>
                      <div>Price:{item.price * item.qty}</div>
                      <div>Brand:{item.brand}</div>
                      <div>Qty:{item.qty}</div>
                      <div>
                        <div className="flex gap-3 cursor-pointer">
                          <button>
                            <PlusCircleFilled
                              onClick={() => handleAdd(item.id)}
                            />
                          </button>
                          <button>
                            <MinusCircleFilled
                              onClick={() => handleMinus(item.id)}
                            />
                          </button>
                        </div>
                      </div>
                    </div>
                    <div>
                      <Button
                        type="primary"
                        danger
                        icon={<DeleteOutlined />}
                        onClick={() => handleDelete(item.id)}
                      >
                        Delete
                      </Button>
                    </div>
                  </div>
                ))}
              </div>
              <div>
                <Button
                  type="primary"
                  className="bg-[green] w-full"
                  onClick={showModal}
                >
                  Order Now
                </Button>
              </div>
              <Modal
                title="Basic Modal"
                open={isModalOpen}
                onOk={handleOk}
                onCancel={handleCancel}
              >
                <div className="flex justify-between">
                  <div>
                    <Avatar icon={<img src="" />} />
                  </div>
                  <div>
                    <Avatar icon={<img src="" />} />
                  </div>
                  <div>
                    <Avatar icon={<img src="" />} />
                  </div>
                </div>
              </Modal>
            </Drawer>
          </div>
        )}
        <div>
          <UserHeader />
        </div>
        {Auth?.map((item) => (
          <div
            key={item.link}
            className="text-white font-bold"
            onClick={() => handleClick(item.link)}
          >
            {item.name}
          </div>
        ))}
      </div>
    </div>
  );
};

export default Index;